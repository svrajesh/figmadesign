(function () {
  'use strict';

  figma.showUI(__html__);
  figma.ui.resize(500, 500);
  figma.ui.onmessage = (msg) => {
    if (msg.type === 'Frame') {
      const selection = figma.currentPage.selection;
      selection.forEach((layer) => {
        console.log(layer);
      });

      figma.currentPage.selection = selection;
    }
    figma.closePlugin();
  };
})();
